package com.druizbarbero.sink

import com.druizbarbero.data.Nucleotide
import com.paulgoldbaum.influxdbclient.Parameter.{Consistency, Precision}
import com.paulgoldbaum.influxdbclient._
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.slf4j.{Logger, LoggerFactory}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * @author diego@intellisense.io
  */
class InfluxDBSink(val measurement: String) extends RichSinkFunction[(Nucleotide, Nucleotide)] {
    val serialVersionUID = - 7303026038406086808L
    val logger: Logger = LoggerFactory.getLogger(getClass)

    @transient var influxDB: InfluxDB = _
    @transient var database: Database = _

    override def open(parameters: Configuration): Unit = {
        super.open(parameters)
        influxDB = InfluxDB.connect("localhost", 8086)
        database = influxDB.selectDatabase("_dna_streams")
    }

    override def invoke(in: (Nucleotide, Nucleotide), context: SinkFunction.Context[_]): Unit = {
        val point = Point(measurement, context.timestamp())
              .addField("5pTo3p", in._1.toString)
              .addField("3pTo5p", in._2.toString)
        val write: Future[Boolean] = database.write(point, precision = Precision.MILLISECONDS, consistency = Consistency.ALL, retentionPolicy = "autogen")
        write.recover{case e: WriteException => logger.error(e.getMessage)}
    }

    override def close(): Unit = {
        influxDB.close()
        super.close()
    }
}
