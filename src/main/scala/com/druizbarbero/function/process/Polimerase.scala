package com.druizbarbero.function.process

import com.druizbarbero.data.Nucleotide
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.streaming.api.scala.DataStream
import org.apache.flink.util.Collector

/**
  * @author diego@intellisense.io
  */
final class Polimerase(@transient val strand: DataStream[Nucleotide]) extends ProcessFunction[Nucleotide, (Nucleotide, Nucleotide)] {

    val fromStrand: String = strand.name

    override def processElement(_5pTo3p: Nucleotide,
                                context: ProcessFunction[Nucleotide, (Nucleotide, Nucleotide)]#Context,
                                out: Collector[(Nucleotide, Nucleotide)]): Unit = {
        if(!fromStrand.contains("leading")) throw new UnsupportedOperationException("Can't help 3pTo5p synthesizing")
        var _3pTo5p = _5pTo3p.pairsWith
        out.collect(_5pTo3p, _3pTo5p)
    }

}
