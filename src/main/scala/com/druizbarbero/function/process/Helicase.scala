package com.druizbarbero.function.process

import com.druizbarbero.data.Nucleotide
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.util.Collector

/**
  * @author diego@intellisense.io
  */
class Helicase(val strand: Int) extends ProcessFunction[(Nucleotide, Nucleotide), Nucleotide] {

    override def processElement(in: (Nucleotide, Nucleotide),
                                context: ProcessFunction[(Nucleotide, Nucleotide), Nucleotide]#Context,
                                out: Collector[Nucleotide]): Unit = out.collect(in.productElement(strand).asInstanceOf[Nucleotide])
}
