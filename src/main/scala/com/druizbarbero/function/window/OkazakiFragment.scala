package com.druizbarbero.function.window

import com.druizbarbero.data.Nucleotide
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow
import org.apache.flink.util.Collector
import org.slf4j.{Logger, LoggerFactory}

/**
  * @author diego@intellisense.io
  */
class OkazakiFragment extends ProcessAllWindowFunction[Nucleotide, Iterable[(Nucleotide, Nucleotide)], GlobalWindow] {
    val logger: Logger = LoggerFactory.getLogger(getClass)

    override def process(context: Context, elements: Iterable[Nucleotide], out: Collector[Iterable[(Nucleotide, Nucleotide)]]): Unit = {
        val fragment = elements.map(_3pTo5p => (_3pTo5p.pairsWith, _3pTo5p))
        logger.debug(String.format("okazaki fragment = %s", fragment.toString()))
        out.collect(fragment)
    }
}
