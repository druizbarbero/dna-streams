package com.druizbarbero.function.window

import java.util.Calendar
import java.util.concurrent.atomic.AtomicLong
import com.druizbarbero.data.Nucleotide
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks
import org.apache.flink.streaming.api.watermark.Watermark

/**
  * @author diego@intellisense.io
  */
class OkazakiTimestampAndWatermarksAssigner extends AssignerWithPunctuatedWatermarks[(Nucleotide, Nucleotide)] {

    val counter = new AtomicLong(1)

    override def extractTimestamp(element: (Nucleotide, Nucleotide), previousElementTimestamp: Long): Long = Calendar.getInstance().getTimeInMillis + counter.getAndAdd(1000)
    override def checkAndGetNextWatermark(lastElement: (Nucleotide, Nucleotide), extractedTimestamp: Long): Watermark = new Watermark(extractedTimestamp)
}
