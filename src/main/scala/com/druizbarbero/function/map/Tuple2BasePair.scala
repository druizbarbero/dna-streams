package com.druizbarbero.function.map

import com.druizbarbero.data._
import org.apache.flink.api.common.functions.RichMapFunction

/**
  * @author diego@intellisense.io
  */
class Tuple2BasePair extends RichMapFunction[(Char, Char), (Nucleotide, Nucleotide)] {

    def map(in: (Char, Char)):(Nucleotide, Nucleotide) = {
        val base1 = in._1 match {
            case 'A'  => Adenine
            case 'C'  => Cytosine
            case 'G'  => Guanine
            case 'T'  => Thymine
        }
        return (base1, base1.pairsWith)
    }
};
