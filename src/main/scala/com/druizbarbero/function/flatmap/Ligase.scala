package com.druizbarbero.function.flatmap

import com.druizbarbero.data.Nucleotide
import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.util.Collector

/**
  * @author diego@intellisense.io
  */
class Ligase extends FlatMapFunction[Iterable[(Nucleotide, Nucleotide)], (Nucleotide, Nucleotide)] {

    override def flatMap(okz: Iterable[(Nucleotide, Nucleotide)], out: Collector[(Nucleotide, Nucleotide)]): Unit = {
        okz.foreach(fragment => out.collect(fragment))
    }
}
