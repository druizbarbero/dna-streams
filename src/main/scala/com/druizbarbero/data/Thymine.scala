package com.druizbarbero.data

/**
  * @author diego@intellisense.io
  */
object Thymine extends Nucleotide {
    override val CAS: String = "65-71-4"
    override val pairsWith: Nucleotide = Adenine
}
