package com.druizbarbero.data

/**
  * @author diego@intellisense.io
  */
trait Nucleotide {
    val CAS: String
    val pairsWith: Nucleotide

    override def toString: String = getClass.getSimpleName.charAt(0).toString
}
