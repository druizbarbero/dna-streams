package com.druizbarbero.data

/**
  * @author diego@intellisense.io
  */
object Guanine extends Nucleotide {
    override val CAS: String = "73-40-5"
    override val pairsWith: Nucleotide = Cytosine
}
