package com.druizbarbero.data

/**
  * @author diego@intellisense.io
  */
object Cytosine extends Nucleotide {
    override val CAS: String = "71-30-7"
    override val pairsWith: Nucleotide = Guanine
}
