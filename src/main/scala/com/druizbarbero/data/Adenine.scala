package com.druizbarbero.data

/**
  * @author diego@intellisense.io
  */
object Adenine extends Nucleotide {
    override val CAS: String = "73-24-5"
    override val pairsWith: Nucleotide = Thymine
}
