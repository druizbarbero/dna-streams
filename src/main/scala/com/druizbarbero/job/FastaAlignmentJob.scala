package com.druizbarbero.job

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment, _}
import org.slf4j.{Logger, LoggerFactory}

/**
  * @author diego@intellisense.io
  */
object FastaAlignmentJob {
    val logger: Logger = LoggerFactory.getLogger(getClass)

    def main(args: Array[String]) {

        val params = ParameterTool.fromArgs(args);
        val env = StreamExecutionEnvironment.getExecutionEnvironment
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
        env.setParallelism(1)

        val source = getClass.getResource("/NC_021153.gbk");
        val dataset: DataStream[String] = env.readTextFile(source.getPath);

        val searchSeq = params.get("searchSeq");
        dataset.map(s => s.split("\\s+"))
                .filter(s => s.length==8)
                .filter(s => s(1).matches("""\d+"""))
                .map(s => (s(1), s(2) + s(3) + s(4) + s(5) + s(6) + s(7)))
                .keyBy(t => t._1)
                .filter(t => t._2.contains(searchSeq))
                .map(r => String.format("*** Result found at %s ***", r))
                .print();

        val jobResults = env.execute("Fasta alignment job ")
        logger.info(String.format("jobResults.getNetRuntime: %s", jobResults.getNetRuntime.toString))
    }
}