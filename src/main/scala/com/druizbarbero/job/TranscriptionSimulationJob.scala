package com.druizbarbero.job

import org.apache.flink.streaming.api.scala._
import com.druizbarbero.data.Nucleotide
import com.druizbarbero.function.flatmap.Ligase
import com.druizbarbero.function.map.Tuple2BasePair
import com.druizbarbero.function.process.{Helicase, Polimerase}
import com.druizbarbero.function.window.{OkazakiFragment, OkazakiTimestampAndWatermarksAssigner}
import com.druizbarbero.sink.InfluxDBSink
import com.druizbarbero.source.DataGenerator
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.slf4j.{Logger, LoggerFactory}

/**
  * @author diego@intellisense.io
  */
object TranscriptionSimulationJob {
    val logger: Logger = LoggerFactory.getLogger(getClass)

    def main(args: Array[String]) {
        val env = StreamExecutionEnvironment.getExecutionEnvironment
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
        env.setParallelism(1)

        val sourceStream: DataStream[(Char, Char)] = env.addSource(DataGenerator)

        val original: DataStream[(Nucleotide, Nucleotide)] = sourceStream.map(new Tuple2BasePair).name("original")
        original.addSink(new InfluxDBSink("original"))

        val leading: DataStream[Nucleotide] = original.process(new Helicase(0)).name("leading")
        val replica1: DataStream[(Nucleotide, Nucleotide)] = leading.process(new Polimerase(leading))
        replica1.addSink(new InfluxDBSink("replica1"))

        val lagging: DataStream[Nucleotide] = original.process(new Helicase(1)).name("lagging")
        val okazakiFragments: DataStream[Iterable[(Nucleotide, Nucleotide)]] = lagging.countWindowAll(10).process(new OkazakiFragment)
        val replica2: DataStream[(Nucleotide, Nucleotide)] = okazakiFragments.flatMap(new Ligase)
                                                                                       .assignTimestampsAndWatermarks(new OkazakiTimestampAndWatermarksAssigner)
        replica2.addSink(new InfluxDBSink("replica2"))

        original.print()

        env.execute("Replication job")
    }
}
