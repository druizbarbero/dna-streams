package com.druizbarbero.source

import java.util.Calendar
import java.util.concurrent.atomic.AtomicInteger
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext
import org.apache.flink.streaming.api.watermark.Watermark
import scala.util.Random

/**
  * @author diego@intellisense.io
  */
object DataGenerator extends SourceFunction[(Char, Char)] {

    @volatile var isRunning = true
    val possible = Set('A', 'C', 'G', 'T')
    val counter = new AtomicInteger(1)

    override def run(context: SourceContext[(Char, Char)]): Unit = {
        while (isRunning) {
            val base1 = Random.alphanumeric.filter(possible).head
            val base2 = base1 match {
                case 'A'  => 'T' case 'T'  => 'A'
                case 'G'  => 'C' case 'C'  => 'G'
            }
            val next = (base1, base2)
            var landmark = Calendar.getInstance().getTimeInMillis + counter.get()
            context.collectWithTimestamp(next, landmark)
            context.emitWatermark(new Watermark(landmark))
            counter.getAndAdd(1000)
        }
    }

    override def cancel(): Unit = isRunning = false
}
